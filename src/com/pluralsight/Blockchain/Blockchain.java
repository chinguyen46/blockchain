package com.pluralsight.Blockchain;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;

public class Blockchain {

    public static ArrayList<Block> blockchain = new ArrayList<Block>();
    public static int difficulty = 5;


    public static void main(String[] args) {

        blockchain.add(new Block("block 1", "0"));
        System.out.println("Trying to mine block 1...");
        blockchain.get(0).mineBlock(difficulty); //get block from index 0 from array list and mine block with difficulty of 5

        blockchain.add(new Block("block 2", blockchain.get(blockchain.size() - 1).hash));
        System.out.println("Trying to mine block 2...");
        blockchain.get(1).mineBlock(difficulty);

        blockchain.add(new Block("block 3", blockchain.get(blockchain.size() - 1).hash));
        System.out.println("Trying to Mine block 3... ");
        blockchain.get(2).mineBlock(difficulty);

        System.out.println("\nBlockchain is Valid: " + isChainValid());


        String blockchainJson = new GsonBuilder().setPrettyPrinting().create().toJson(blockchain); //turns output to JSON
        System.out.println(blockchainJson);
        System.out.println("\nThe block chain: ");
        System.out.println(blockchainJson);


//        Block genesisBlock = new Block ("First block", "0");
//        System.out.println("Hash for block 1 is: " + genesisBlock.hash);
//
//        Block block2 = new Block ("Second block", genesisBlock.hash);
//        System.out.println("Hash for block 2 is: " + block2.hash);
//
//        Block block3 = new Block ("Third block", genesisBlock.hash);
//        System.out.println("Hash for block 3 is " + block3.hash);


    }

    public static Boolean isChainValid() {
        Block currentBlock; //of type Block
        Block previousBlock; //of type Block

        for (int i = 1; i < blockchain.size(); i++) {
            currentBlock = blockchain.get(i); //store data of array into type block
            previousBlock = blockchain.get(i - 1); //store data of array into type block

            if (!currentBlock.hash.equals(currentBlock.calculateHash())) {
                System.out.println("current hashes are not equal");
                return false;
            }

            if (!previousBlock.hash.equals(previousBlock.calculateHash())) {
                System.out.println("previous hash are not equal");
                return false;
            }

        }
        return true;
    }

    public static void addBlock(Block newBlock) {
        newBlock.mineBlock(difficulty);
        blockchain.add(newBlock);

    }
}



